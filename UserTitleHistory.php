<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTitleHistory extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_title_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'from_title_id', 'to_title_id'];

    /**
     * A title history belongs to a user
     * 
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    /**
     * A from title history belongs to a title
     * 
     * @return BelongsTo
     */
    public function from()
    {
        return $this->belongsTo('App\Title', 'from_title_id');
    }

    /**
     * A to title history belongs to a title
     * 
     * @return BelongsTo
     */
    public function to()
    {
        return $this->belongsTo('App\Title', 'to_title_id');
    }

}
