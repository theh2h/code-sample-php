<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Product
 * @package App
 */
class Product extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'therapy_area_id'];

    /**
     * A product can belong to many users
     *
     * @return BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * A product belongs to a therapy area
     *
     * @return BelongsTo
     */
    public function therapy_area()
    {
        return $this->belongsTo('App\TherapyArea');
    }

    /**
     * A product has many questions
     *
     * @return HasMany
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    /**
     * A product has many assessments
     *
     * @return HasMany
     */
    public function assessments()
    {
        return $this->hasMany('App\Assessment');
    }

}
