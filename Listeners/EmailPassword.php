<?php

namespace App\Listeners;

use App\Events\UserStored;
use Mail;

class EmailPassword {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserStored  $event
     * @return void
     */
    public function handle(UserStored $event)
    {
        $user = $event->user;
        Mail::send('emails.UserStored', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('User Registration !');
        });
    }

}
