<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Title
 * @package App
 */
class Title extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'titles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * A title has many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * A title has many excluded questions
     *
     * @return BelongsToMany
     */
    public function questions_excluded()
    {
        return $this->belongsToMany('App\Question', 'question_excluded_titles')->withTimestamps();
    }

    /**
     * A title has many excluded questions
     *
     * @return BelongsToMany
     */
    public function assessments_excluded()
    {
        return $this->belongsToMany('App\Assessment', 'assessment_excluded_titles')->withTimestamps();
    }

}
