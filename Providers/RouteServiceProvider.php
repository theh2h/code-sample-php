<?php

namespace App\Providers;

use App\User;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use function app_path;

class RouteServiceProvider extends ServiceProvider {

    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);

        $router->bind('users', function($value) {
            return User::withTrashed()->where('id', $value)->first();
        });
        $router->model('products', 'App\Product');
        $router->model('library_categories', 'App\LibraryCategory');
        $router->model('library_files', 'App\LibraryFile');
        $router->model('question_categories', 'App\QuestionCategory');
        $router->model('questions', 'App\Question');
        $router->model('assessments', 'App\Assessment');
    }

    /**
     * Define the routes for the application.
     *
     * @param  Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }

}
