<?php

namespace App\Providers;

use App\User;
use App\UserTitleHistory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // USER events
        User::updated(function ($user) {
            $original = $user->getOriginal();
            if ($user->title_id != $original['title_id']) {
                UserTitleHistory::create(['user_id' => $user->id, 'from_title_id' => $original['title_id'], 'to_title_id' => $user->title_id]);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
