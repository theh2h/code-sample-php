<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class QuestionCategory extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'question_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * A question category can have many questions
     *
     * @return HasMany
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    /**
     * A question cateogry is related to many assessments
     *
     * @return BelongsToMany
     */
    public function assessment_weights()
    {
        return $this->belongsToMany('App\Assessment', 'assessment_question_category_weights')->withTimestamps()->withPivot('weight');
    }

}
