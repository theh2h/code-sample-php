<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProductsHistory extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_products_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'product_id', 'action'];

    /**
     * A product history belongs to a user
     * 
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    /**
     * A product history belongs to a product
     * 
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

}
