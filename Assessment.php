<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'assessments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'duration', 'number_of_questions', 'start', 'end', 'product_id'];

    /**
     * An assessment belongs to a product
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /**
     * An assessment has many questions
     *
     * @return BelongsToMany
     */
    public function questions()
    {
        return $this->belongsToMany('App\Question', 'assessment_question')->withTimestamps();
    }

    /**
     * An assessment has many excluded titles
     *
     * @return BelongsToMany
     */
    public function excluded_titles()
    {
        return $this->belongsToMany('App\Title', 'assessment_excluded_titles')->withTimestamps();
    }

    /**
     * An assessment has many excluded countries
     *
     * @return BelongsToMany
     */
    public function excluded_countries()
    {
        return $this->belongsToMany('App\Country', 'assessment_excluded_countries')->withTimestamps();
    }

    /**
     * An assessment has many different weight distributions according to the question category
     *
     * @return BelongsToMany
     */
    public function question_categories_weights()
    {
        return $this->belongsToMany('App\QuestionCategory', 'assessment_question_category_weights')->withTimestamps()->withPivot('weight');
    }

}
