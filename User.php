<?php

namespace App;

use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword,
        \Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'employee_id', 'country_id', 'title_id', 'department_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['is_super_admin', 'is_accounts_admin', 'is_assessments_admin', 'is_statistics_admin', 'subordinates'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Set the user's password.
     *
     * @param  string  $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Set the user's name.
     *
     * @param  string  $value
     * @return string
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    /**
     * A user can have a senior
     * 
     * @return HasOne
     */
    public function senior()
    {
        return $this->hasOne('App\User', 'id', 'senior_id');
    }

    /**
     * A user belongs to a title
     * 
     * @return BelongsTo
     */
    public function title()
    {
        return $this->belongsTo('App\Title');
    }

    /**
     * A user belongs to a department
     * 
     * @return BelongsTo
     */
    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    /**
     * A user belongs to a country
     *
     * @return BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * A user can have many roles
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    /**
     * A user can have many products
     *
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Product')->withTimestamps();
    }

    /**
     * A user has a history of product changes
     *
     * @return HasMany
     */
    public function products_history()
    {
        return $this->hasMany('App\UserProductsHistory');
    }

    /**
     * Update user history for product changes
     *
     */
    public function UpdateProductHistory($arrayToAdd = [])
    {
        $arrayToAdd = Collection::make($arrayToAdd);
        $arrayToDestroy = $this->products()->lists('product_id');

        foreach ($arrayToDestroy as $keyToDestroy => $valueToDestroy) {
            foreach ($arrayToAdd as $keyToAdd => $valueToAdd) {
                if ($valueToDestroy === $valueToAdd) {
                    $arrayToDestroy->forget($keyToDestroy);
                    $arrayToAdd->forget($keyToAdd);
                }
            }
        }

        foreach ($arrayToDestroy as $valueToDestroy) {
            UserProductsHistory::create(['user_id' => $this->id, 'product_id' => $valueToDestroy, 'action' => 0]);
        }
        foreach ($arrayToAdd as $valueToAdd) {
            UserProductsHistory::create(['user_id' => $this->id, 'product_id' => $valueToAdd, 'action' => 1]);
        }
    }

    /**
     * A user has a history of title changes
     *
     * @return HasMany
     */
    public function titles_history()
    {
        return $this->hasMany('App\UserTitleHistory');
    }

    /**
     * get the history of this user
     *
     * @return \Illuminate\Support\Collection
     */
    public function generateHistory()
    {
        $productsHistory = $this->products_history;
        $titlesHistory = $this->titles_history;

        $history = new Collection;
        $temp = [];

        foreach ($productsHistory as $record) {
            if ($record->action == 0) {
                $temp['message'] = $record->user->name . ' is no longer assigned to ' . $record->product->name . '.';
            } else {
                $temp['message'] = $record->user->name . ' has been assigned to ' . $record->product->name . '.';
            }
            $temp['created_at'] = $record->created_at->format('Y-m-d H:i:s');
            $temp['time'] = $record->created_at->format('d M,Y - h:i A');
            $history->push((object) $temp);
        }

        foreach ($titlesHistory as $record) {
            $temp['message'] = $record->user->name . "'s title has been changed to " . $record->to->name . '.';
            $temp['created_at'] = $record->created_at->format('Y-m-d H:i:s');
            $temp['time'] = $record->created_at->format('d M,Y - h:i A');
            $history->push((object) $temp);
        }

        return $history->sortBy('created_at')->values()->all();
    }

    /**
     * check if user is Super Admin
     * 
     * @return boolean
     */
    public function isSuperAdmin()
    {
        return $this->roles()->where('role_id', 1)->exists();
    }

    /**
     * check if user is Account Admin
     * 
     * @return boolean
     */
    public function isAccountsAdmin()
    {
        return $this->roles()->where('role_id', 2)->exists();
    }

    /**
     * check if user is Assessment Admin
     * 
     * @return boolean
     */
    public function isAssessmentsAdmin()
    {
        return $this->roles()->where('role_id', 3)->exists();
    }

    /**
     * check if user is statistics Admin
     * 
     * @return boolean
     */
    public function isStatisticsAdmin()
    {
        return $this->roles()->where('role_id', 4)->exists();
    }

    /**
     * get the subordinates of this user
     *
     * @return \Illuminate\Database\Eloquent\Collection;
     */
    public function getSubordinatesAttribute()
    {
        return $this->where('senior_id', $this->id)->get(['id', 'name']);
    }

    /**
     * Check if user is super admin.
     *
     * @return bool
     */
    public function getIsSuperAdminAttribute()
    {
        return $this->isSuperAdmin();
    }

    /**
     * Check if user is accounts admin.
     *
     * @return bool
     */
    public function getIsAccountsAdminAttribute()
    {
        return $this->isAccountsAdmin();
    }

    /**
     * Check if user is assessments admin.
     *
     * @return bool
     */
    public function getIsAssessmentsAdminAttribute()
    {
        return $this->isAssessmentsAdmin();
    }

    /**
     * Check if user is statistics admin.
     *
     * @return bool
     */
    public function getIsStatisticsAdminAttribute()
    {
        return $this->isStatisticsAdmin();
    }

}
