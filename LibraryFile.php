<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryFile extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'library_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['file_name', 'file_path', 'library_category_id'];

    /**
     * A library file belongs to a library category
     * 
     * @return BelongsTo
     */
    public function library_category()
    {
        return $this->belongsTo('App\LibraryCategory');
    }

}
