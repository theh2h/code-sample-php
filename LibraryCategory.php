<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryCategory extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'library_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * A library category belongs to a library category
     * 
     * @return BelongsTo
     */
    public function parent_category()
    {
        return $this->belongsTo('App\LibraryCategory', 'parent_id', 'id');
    }

    /**
     * A library category has many library categories
     * 
     * @return HasMany
     */
    public function child_categories()
    {
        return $this->hasMany('App\LibraryCategory', 'parent_id', 'id');
    }

    /**
     * A library category has many library files
     * 
     * @return HasMany
     */
    public function files()
    {
        return $this->hasMany('App\LibraryFile');
    }

}
