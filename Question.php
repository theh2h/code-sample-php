<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Question extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['text', 'hint', 'product_id', 'question_category_id'];

    /**
     * A question belongs to a question category
     *
     * @return BelongsTo
     */
    public function question_category()
    {
        return $this->belongsTo('App\QuestionCategory');
    }

    /**
     * A question belongs to a product
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /**
     * A question belongs to many assessments
     *
     * @return BelongsToMany
     */
    public function assessments()
    {
        return $this->belongsToMany('App\Assessment', 'assessment_question')->withTimestamps();
    }

    /**
     * A question has many possible answers
     *
     * @return HasMany
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * A question has many excluded titles
     *
     * @return BelongsToMany
     */
    public function excluded_titles()
    {
        return $this->belongsToMany('App\Title', 'question_excluded_titles')->withTimestamps();
    }

    /**
     * A question has many excluded countries
     *
     * @return BelongsToMany
     */
    public function excluded_countries()
    {
        return $this->belongsToMany('App\Country', 'question_excluded_countries')->withTimestamps();
    }

}
