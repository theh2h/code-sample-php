<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'api'], function() {
    //users
    Route::post('user/authenticate', ['uses' => 'UsersController@authenticate']);
    Route::get('user/authenticate', ['uses' => 'UsersController@getAuthenticatedUser']);
    Route::get('users/lists', ['uses' => 'UsersController@lists']);
    Route::delete('users/{users}/disable', ['uses' => 'UsersController@disable']);
    Route::post('users/{users}/restore', ['uses' => 'UsersController@restore']);
    Route::patch('users/{users}/updatePassword', ['uses' => 'UsersController@updatePassword']);
    Route::resource('users', 'UsersController');

    //products
    Route::get('products/lists', ['uses' => 'ProductsController@lists']);
    Route::resource('products', 'ProductsController');

    //library
    Route::resource('library_categories', 'LibraryCategoriesController');

    //library files
    Route::resource('library_files', 'LibraryFilesController');

    //question categories 
    Route::resource('question_categories', 'QuestionCategoriesController');

    //questions
    Route::get('products/{products}/questions', ['uses' => 'QuestionsController@index']);
    Route::resource('questions', 'QuestionsController');

    //assessments
    Route::get('assessments', ['uses' => 'AssessmentsController@indexByProducts']);
    Route::patch('products/{products}/assessments/{assessments}/schedule', ['uses' => 'AssessmentsController@updateSchedule']);
    Route::resource('products.assessments', 'AssessmentsController');

    Route::get('titles/lists', ['uses' => 'TitlesController@lists']);
    Route::get('countries/lists', ['uses' => 'CountriesController@lists']);
    Route::get('departments/lists', ['uses' => 'DepartmentsController@lists']);
    Route::get('roles/lists', ['uses' => 'RolesController@lists']);
});
