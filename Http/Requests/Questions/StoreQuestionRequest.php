<?php

namespace App\Http\Requests\Questions;

use App\Http\Requests\Request;
use Validator;

class StoreQuestionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'text' => 'required|string',
            'hint' => 'string',
            'question_category_id' => 'required',
            'correct' => 'required'
        ];

        return $rules;
    }

    /**
     * Get the parent valdiator and add a custom validation rule for the answers array 
     * answers array must have at least 2 filled entries
     * 
     * @return Validator
     */
    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {

            $answers = $this->request->get('answers');
            $count = 0;

            foreach ($answers as $key => $val) {
                //trim the value for white spaces
                $strTemp = trim($val);
                if ($strTemp !== '') {
                    $count++;
                }
            }

            //check if at least 2 answers are filled
            if ($count < 2 || $count > 6) {
                $validator->errors()->add('answers', 'At least 2 answers must be filled');
            }

            //check if the checked correct answer is not empty
            if ($this->has('correct')) {
                if (trim($answers[$this->request->get('correct')]) === '') {
                    $validator->errors()->add('correct', 'The correct answer cannot be an empty one');
                }
            }
        });

        return $validator;
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $messages['correct.required'] = 'You need to choose the correct answer.';

        return $messages;
    }

}
