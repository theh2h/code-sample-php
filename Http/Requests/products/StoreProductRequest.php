<?php

namespace App\Http\Requests\products;

use App\Http\Requests\Request;

class StoreProductRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'therapy_area_id' => 'required|integer|exists:therapy_areas,id',
            'image' => 'image',
        ];
    }

}
