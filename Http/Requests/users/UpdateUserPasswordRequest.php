<?php

namespace App\Http\Requests\users;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;

class UpdateUserPasswordRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oldPassword' => 'required',
            'newPassword' => 'required|same:confirmPassword',
            'confirmPassword' => 'required',
        ];
    }

    /**
     * Get the parent valdiator and add a custom validation rule for the questions array 
     * questions must not be under 30
     * 
     * @return Validator
     */
    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {

//            $auth = new JWTAuth();
//            $user = $auth->parseToken()->authenticate();

            $user = JWTAuth::parseToken()->authenticate();

            if (!Hash::check($this->oldPassword, $user->password)) {
                $validator->errors()->add('oldPassword', 'Old password is incorrect !');
            }
        });

        return $validator;
    }

}
