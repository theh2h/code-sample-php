<?php

namespace App\Http\Requests\Assessments;

use App\Http\Requests\Request;

class UpdateAssessmentScheduleRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
//            'name' => 'required|string|between:3,50',
//            'duration' => 'required|integer|between:5,360',
        ];

        return $rules;
    }

}
