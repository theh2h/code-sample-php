<?php

namespace App\Http\Requests\Assessments;

use App\Http\Requests\Request;
use Validator;

class UpdateAssessmentRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|between:3,50',
            'duration' => 'required|integer|between:5,360',
        ];

        return $rules;
    }

    /**
     * Get the parent valdiator and add a custom validation rule for the questions array 
     * questions must not be under 30
     * 
     * @return Validator
     */
    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {

            $questions = $this->request->get('questions');
            $count = count($questions);

            if ($count < 30) {
                $validator->errors()->add('questions', 'At least 30 questions must be chosen for the assessment');
            }
        });

        return $validator;
    }

}
