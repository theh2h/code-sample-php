<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionCategories\StoreQuestionCategoryRequest;
use App\Http\Requests\QuestionCategories\UpdateQuestionCategoryRequest;
use App\QuestionCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use SebastianBergmann\RecursionContext\Exception;
use Symfony\Component\HttpFoundation\Response;
use function response;

class QuestionCategoriesController extends Controller {

    public function __construct()
    {
// Apply the jwt.auth middleware to all methods in this controller
// except for the authenticate method. We don't want to prevent
// the user from retrieving their token if they don't already have it
//        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $questionCategories = QuestionCategory::all();
            return response()->json(['questionCategories' => $questionCategories], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(StoreQuestionCategoryRequest $request)
    {
        try {

            $questionCategory = new QuestionCategory;
            $questionCategory->fill($request->all());
            $questionCategory->save();

            return response()->json(['questionCategory' => $questionCategory], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(QuestionCategory $questionCategory)
    {
        try {
            return response()->json(['questionCategory' => $questionCategory], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateQuestionCategoryRequest $request, QuestionCategory $questionCategory)
    {
        try {

            $questionCategory->fill($request->all());
            $questionCategory->save();

            return response()->json(['questionCategory' => $questionCategory], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(QuestionCategory $questionCategory)
    {
        try {

            $questionCategory->delete();

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

}
