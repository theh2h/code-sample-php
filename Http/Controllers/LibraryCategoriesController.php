<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\LibraryCategories\StoreLibraryCategoryRequest;
use App\Http\Requests\LibraryCategories\UpdateLibraryCategoryRequest;
use App\LibraryCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use SebastianBergmann\RecursionContext\Exception;
use Symfony\Component\HttpFoundation\Response;
use function response;

class LibraryCategoriesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $libraryCategories = LibraryCategory::whereNull('parent_id')->with('parent_category', 'child_categories.files', 'files')->get();
            return response()->json(['libraryCategories' => $libraryCategories], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(StoreLibraryCategoryRequest $request)
    {
        try {


            $libraryCategory = new LibraryCategory;
            $libraryCategory->fill($request->all());
            if ($request->parent != NULL) {
                $libraryCategory->parent_id = $request->parent['id'];
            }
            $libraryCategory->save();

            return response()->json(['libraryCategory' => $libraryCategory->load('parent_category', 'child_categories.files', 'files')], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(LibraryCategory $libraryCategory)
    {
        try {

            $libraryCategory->load('parent_category', 'child_categories.files', 'files');

            return response()->json(['libraryCategory' => $libraryCategory], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateLibraryCategoryRequest $request, LibraryCategory $libraryCategory)
    {
        try {

            $libraryCategory->fill($request->all());
            $libraryCategory->save();

            return response()->json(['libraryCategory' => $libraryCategory->load('parent_category', 'child_categories.files', 'files')], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(LibraryCategory $libraryCategory)
    {
        try {
            $libraryCategory->delete();

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

}
