<?php

namespace App\Http\Controllers;

use App\Country;
use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\users\StoreUserRequest;
use App\Http\Requests\users\UpdateUserPasswordRequest;
use App\Http\Requests\users\UpdateUserRequest;
use App\Product;
use App\Role;
use App\Title;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use function response;
use function str_random;

class UsersController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $users = User::with('title', 'department', 'products', 'roles', 'senior')->get();
            return response()->json(['users' => $users], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display information for storing.
     *
     * @return Response
     */
    public function create()
    {
        try {

            $titles = Title::get(['name', 'id']);
            $roles = Role::get(['name', 'id']);
            $users = User::whereNull('senior_id')->get(['name', 'id']);
            $countries = Country::get(['name', 'id']);
            $products = Product::get(['name', 'id']);
            $departments = Department::get(['name', 'id']);

            return response()->json(compact(['titles', 'roles', 'users', 'countries', 'products', 'departments']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreUserRequest $request)
    {

        try {
            $user = new User;
            $user->fill($request->all());
            $user->password = str_random(10);
            $user->active = 0;
            $user->save();


            $user->products()->sync($request->products);
            $user->roles()->sync($request->roles);

            foreach ($request->subordinates as $subordinateID) {
                $subordinate = User::findOrFail($subordinateID);
                $subordinate->senior_id = $user->id;
                $subordinate->save();
            }

//            Event::fire(new UserStored($user));

            return response()->json(['user' => $user->load('title', 'department', 'products', 'roles', 'senior')], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(User $user)
    {
        try {

            $userHistory = $user->generateHistory();

            $tempUser = Collection::make($user->load('title', 'senior', 'country', 'department', 'products', 'roles', 'senior'));

            $tempUser->put("history", $userHistory);
            $tempUser->forget('products_history');
            $tempUser->forget('titles_history');

            return response()->json(['user' => $tempUser], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display information for updating.
     *
     * @return Response
     */
    public function edit(User $user)
    {
        try {


            $user->load('title', 'department', 'products', 'roles', 'senior');
            $titles = Title ::get(['name', 'id']);
            $roles = Role::get(['name', 'id']);
            $users = User::where('id', '!=', $user->id)->whereNull('senior_id')->orWhere('senior_id', $user->id)->get(['name', 'id']);
            $countries = Country::get(['name', 'id']);
            $products = Product::get(['name', 'id']);
            $departments = Department::get(['name', 'id']);

            return response()->json(compact(['user', 'titles', 'roles', 'users', 'countries', 'products', 'departments']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json([ 'error' => $e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        try {

            $user->fill($request->all());
            $user->save();

            $user->UpdateProductHistory($request->products);

            $user->products()->sync($request->products);
            $user->roles()->sync($request->roles);

            foreach ($user->subordinates as $subordinate) {
                $subordinate = User::findOrFail($subordinate->id);
                $subordinate->senior_id = null;
                $subordinate->save();
            }

            foreach ($request->subordinates as $subordinateID) {
                $subordinate = User::findOrFail($subordinateID);
                $subordinate->senior_id = $user->id;
                $subordinate->save();
            }

            return response()->json([ 'user' => $user->load('title', 'department', 'products', 'roles', 'senior')], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Update the specified resource in storage for a specific attribute.
     *
     * @param  int  $id
     * @return Response
     */
    public function updatePassword(UpdateUserPasswordRequest $request, User $user)
    {
        try {

            $user->password = $request->newPassword;

            if ($user->active == 0) {
                $user->active = 1;
            }

            $user->save();

            return response()->json([ 'user' => $user], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(User $user)
    {
        try {

            $user->forceDelete();

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Disable the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function disable(User $user)
    {
        try {

            $user->delete();

            return response()->json(['user' => $user->load('title', 'department', 'products', 'roles', 'senior')], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function restore(User $user)
    {
        try {

            $user->restore();

            return response()->json(['user' => $user->load('title', 'department', 'products', 'roles', 'senior')], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Authenticates a user with JWT and sends back the token if successful
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'
        ));
    }

    /**
     * Returns an authenticated user object
     *
     * @return Response
     */
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'
        ));
    }

    /**
     * lists name and id of resource.
     *
     * @return Response
     */
    public function lists()
    {
        try {
            $users = User::get([ 'name', 'id']);
            return response()->json(['users' => $users], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

}
