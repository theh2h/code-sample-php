<?php

namespace App\Http\Controllers;

use App\Assessment;
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assessments\StoreAssessmentRequest;
use App\Http\Requests\Assessments\UpdateAssessmentRequest;
use App\Http\Requests\Assessments\UpdateAssessmentScheduleRequest;
use App\Product;
use App\QuestionCategory;
use App\Title;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use SebastianBergmann\RecursionContext\Exception;
use Symfony\Component\HttpFoundation\Response;

class AssessmentsController extends Controller {

    public function __construct()
    {
// Apply the jwt.auth middleware to all methods in this controller
// except for the authenticate method. We don't want to prevent
// the user from retrieving their token if they don't already have it
//        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Displays a listing of the resource grouped by its products
     * 
     * @return Response
     */
    public function indexByProducts()
    {
        try {

            $assessments = Product::with('assessments')->get();

            return response()->json(compact(['assessments']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Product $product)
    {
        try {

            if ($product->exists) {
                $assessments = $product->assessments;
            } else {
                $assessments = Assessment::all();
            }

            return response()->json(['assessments' => $assessments], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Product $product)
    {
        try {

            $titles = Title::get(['name', 'id']);
            $countries = Country::get(['name', 'id']);
            $questionCategories = QuestionCategory::get(['name', 'id']);
            $questions = $product->questions()->with('answers', 'question_category')->get();

            return response()->json(compact('product', 'titles', 'countries', 'questionCategories', 'questions'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(StoreAssessmentRequest $request, Product $product)
    {

        try {

            $assessment = $product->assessments()->create($request->all());

            $assessment->question_categories_weights()->sync($request->question_categories_weights);
            $assessment->questions()->sync($request->questions);
            $assessment->excluded_countries()->sync($request->excluded_countries);
            $assessment->excluded_titles()->sync($request->excluded_titles);

            return response()->json(compact(['assessment']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Product $product, Assessment $assessment)
    {
        try {



            return response()->json(compact(['']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Product $product, Assessment $assessment)
    {
        try {

            $titles = Title::get(['name', 'id']);
            $countries = Country::get(['name', 'id']);
            $questionCategories = QuestionCategory::get(['name', 'id']);
            $questions = $product->questions()->with('answers', 'question_category')->get();
            $assessment->load('questions', 'question_categories_weights', 'excluded_titles', 'excluded_countries');



            return response()->json(compact('product', 'titles', 'countries', 'questionCategories', 'questions', 'assessment'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateAssessmentRequest $request, Product $product, Assessment $assessment)
    {
        try {

            $assessment->update($request->all());

            $assessment->question_categories_weights()->sync($request->question_categories_weights);
            $assessment->questions()->sync($request->questions);
            $assessment->excluded_countries()->sync($request->excluded_countries);
            $assessment->excluded_titles()->sync($request->excluded_titles);

            return response()->json(compact(['assessment']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Update the specified resource in storage for a specific attribute.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function updateSchedule(UpdateAssessmentScheduleRequest $request, Product $product, Assessment $assessment)
    {
        try {

            $assessment->start = $request->start;
            $assessment->end = $request->end;
            $assessment->save();

            return response()->json(compact(['assessment']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Product $product, Assessment $assessment)
    {
        try {

            $assessment->delete();

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

}
