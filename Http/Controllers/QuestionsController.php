<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Requests\Questions\StoreQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Product;
use App\Question;
use App\QuestionCategory;
use App\Title;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use SebastianBergmann\RecursionContext\Exception;
use Symfony\Component\HttpFoundation\Response;
use function response;

class QuestionsController extends Controller {

    public function __construct()
    {
// Apply the jwt.auth middleware to all methods in this controller
// except for the authenticate method. We don't want to prevent
// the user from retrieving their token if they don't already have it
//        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Product $product)
    {
        try {

            if ($product->exists) {
                $questions = $product->questions()->with('answers', 'question_category', 'product')->get();
            } else {
                $questions = Question::with('answers', 'question_category', 'product')->get();
            }

            return response()->json(['questions' => $questions], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        try {

            $titles = Title::get(['name', 'id']);
            $countries = Country::get(['name', 'id']);
            $questionCategories = QuestionCategory::get(['name', 'id']);

            return response()->json(compact('product', 'titles', 'countries', 'questionCategories'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(StoreQuestionRequest $request)
    {
        try {



            $question = Question::create($request->all());

            foreach ($request->answers as $key => $answer) {
                if (trim($answer) !== '') {
                    if ($key == $request->correct) {
                        $question->answers()->create(['text' => $answer, 'correct' => 1]);
                    } else {
                        $question->answers()->create(['text' => $answer]);
                    }
                }
            }

            $question->excluded_countries()->sync($request->excluded_countries);
            $question->excluded_titles()->sync($request->excluded_titles);

            $question->load('answers', 'question_category', 'product');

            return response()->json(compact('question'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Question $question)
    {
        try {

            $titles = Title::get(['name', 'id']);
            $countries = Country::get(['name', 'id']);
            $questionCategories = QuestionCategory::get(['name', 'id']);
            $question->load('answers', 'question_category', 'product', 'excluded_titles', 'excluded_countries');


            return response()->json(compact('product', 'titles', 'countries', 'questionCategories', 'question'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
        try {
            $question->update($request->all());

            $question->answers()->delete();

            foreach ($request->answers as $key => $answer) {
                if (trim($answer) !== '') {
                    if ($key == $request->correct) {
                        $question->answers()->create(['text' => $answer, 'correct' => 1]);
                    } else {
                        $question->answers()->create(['text' => $answer]);
                    }
                }
            }

            $question->excluded_countries()->sync($request->excluded_countries);
            $question->excluded_titles()->sync($request->excluded_titles);

            $question->load('answers', 'question_category', 'product');

            return response()->json(compact('question'), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Question $question)
    {
        try {

            $question->delete();

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

}
