<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\products\StoreProductRequest;
use App\Http\Requests\products\UpdateProductRequest;
use App\Product;
use App\TherapyArea;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use SebastianBergmann\RecursionContext\Exception;
use Symfony\Component\HttpFoundation\Response;
use function response;

class ProductsController extends Controller {

    public function __construct()
    {
// Apply the jwt.auth middleware to all methods in this controller
// except for the authenticate method. We don't want to prevent
// the user from retrieving their token if they don't already have it
//        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $products = Product::with('therapy_area')->get();
            return response()->json(['products' => $products], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        try {

            $therapyAreas = TherapyArea::get(['name', 'id']);

            return response()->json(compact(['therapyAreas']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(StoreProductRequest $request)
    {
        try {


            $product = new Product;
            $product->fill($request->all());

            if ($request->hasFile('image')) {
                $destinationPath = 'uploads/img/products';
                $original_file_name = $request->file('image')->getClientOriginalName();
                $new_file_name = str_random(30) . $original_file_name;
                $save_proccess = $request->file('image')->move($destinationPath, $new_file_name);
                if ($save_proccess) {
                    $product->image_name = $original_file_name;
                    $product->image_path = $destinationPath . '/' . $new_file_name;
                }
            }

            $product->save();

            return response()->json(['product' => $product->load('therapy_area')], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Product $product)
    {
        try {

            $product->load('therapy_area');
            $therapyAreas = TherapyArea::get(['name', 'id']);

            return response()->json(compact(['product', 'therapyAreas']), 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        try {


            $product->fill($request->all());

            if ($request->hasFile('image')) {
                $destinationPath = 'uploads/img/products';
                $original_file_name = $request->file('image')->getClientOriginalName();
                $new_file_name = str_random(30) . $original_file_name;
                $save_proccess = $request->file('image')->move($destinationPath, $new_file_name);
                if ($save_proccess) {
                    unlink($product->image_path);
                    $product->image_name = $original_file_name;
                    $product->image_path = $destinationPath . '/' . $new_file_name;
                }
            }

            $product->save();

            return response()->json(['product' => $product->load('therapy_area')], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Product $product)
    {
        try {
            unlink($product->image_path);
            $product->delete();

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * lists name and id of resource.
     *
     * @return Response
     */
    public function lists()
    {
        try {
            $products = Product::get([ 'name', 'id']);
            return response()->json(['products' => $products], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        }
    }

}
