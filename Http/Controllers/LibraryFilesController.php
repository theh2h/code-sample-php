<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\LibraryFiles\StoreLibraryFileRequest;
use App\LibraryFile;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use SebastianBergmann\RecursionContext\Exception;
use Symfony\Component\HttpFoundation\Response;
use function response;
use function str_random;

class LibraryFilesController extends Controller {

    public function __construct()
    {
// Apply the jwt.auth middleware to all methods in this controller
// except for the authenticate method. We don't want to prevent
// the user from retrieving their token if they don't already have it
//        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(StoreLibraryFileRequest $request)
    {
        try {

            $files = new Collection;

            $destinationPath = 'uploads/files/library';

            foreach ($request->file('files') as $file) {
                $original_file_name = $file->getClientOriginalName();
                $new_file_name = str_random(30) . $original_file_name;
                $save_proccess = $file->move($destinationPath, $new_file_name);
                if ($save_proccess) {
                    $libraryFile = new LibraryFile;
                    $libraryFile->library_category_id = $request->library_category_id;
                    $libraryFile->file_name = $original_file_name;
                    $libraryFile->file_path = $destinationPath . '/' . $new_file_name;
                    $libraryFile->save();
                    $files->push($libraryFile);
                }
            }

            return response()->json(['files' => $files], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(LibraryFile $file)
    {
        try {
            return response()->download($file->file_path);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(LibraryFile $file)
    {
        try {

            unlink($file->file_path);
            $file->delete();

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 500);
        } catch (QueryException $e) {
            return response()->json(['error' => $e], 500);
        }
    }

}
