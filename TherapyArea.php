<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductCategory
 * @package App
 */
class TherapyArea extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'therapy_areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * A therapy area has many products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }

}
