<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Answer extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['text', 'correct', 'question_id'];

    /**
     * An answer belongs to a question
     *
     * @return BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    /**
     * Scope a query to get the correct answer.
     *
     * @return Builder
     */
    public function scopeCorrect($query)
    {
        return $query->where('correct', 1);
    }

}
