<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 * @package App
 */
class Country extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * A country has many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * A country has many excluded questions
     *
     * @return BelongsToMany
     */
    public function questions_excluded()
    {
        return $this->belongsToMany('App\Question', 'question_excluded_titles')->withTimestamps();
    }

    /**
     * A country has many excluded assessments
     *
     * @return BelongsToMany
     */
    public function assessments_excluded()
    {
        return $this->belongsToMany('App\Assessment', 'assessment_excluded_countries')->withTimestamps();
    }

}
